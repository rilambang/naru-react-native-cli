import React from "react";
import {
  AppRegistry, View, StyleSheet, StatusBar, Image, Linking, TouchableHighlight, Clipboard,
  ToastAndroid,
  AlertIOS,
  Platform,
  TouchableOpacity,
  ImageBackground,
  Dimensions
} from "react-native";
import {
  Text,
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Left,
  Badge,
  Right,
  Icon,
  Title,
  Input,
  InputGroup,
  Item,
  Tab,
  Tabs,
  Footer,
  FooterTab,
  Label,
  Thumbnail,
  ListItem,
  CheckBox,
  Picker,
  Grid,
  Row,
  Col
} from "native-base";
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import { AppHeader, AppFooter } from '../app-nav/index';

import Modal from 'react-native-modal';

import Share, { ShareSheet, Button } from 'react-native-share';
var Analytics = require('react-native-firebase-analytics');

const ITEM_HEIGHT = Dimensions.get('window').height;

export default class newsDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      databerita: '',
      id_berita: this.props.navigation.state.params.idBerita,
      shareFacebook: '',
      shareTwitter: '',
      shareEmail: [],
      visible: false,
      shareWhatsapp: [],
      isModalVisible: false,
      modalMargin: '',
      heightModal : "100%"
    };
  }

  componentWillMount() {
    Analytics.logEvent('news_detail', {
      'item_id': this.state.id_berita
    });
  }

  onCancel() {
    this.setState({ visible: false });
  }
  onOpen() {
    this.setState({ visible: true });
  }
  componentDidMount() {
    fetch("http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000/news_event_id/" + this.state.id_berita, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then((data) => {
        this.setState({
          databerita: data
        });
      })

    fetch("http://ec2-13-250-50-44.ap-southeast-1.compute.amazonaws.com:9009/api/shareFacebook", {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
      },
    })
      .then(response => response.json())
      .then((data) => {
        this.setState({
          shareFacebook: data.data
        });
      })

    fetch("http://ec2-13-250-50-44.ap-southeast-1.compute.amazonaws.com:9009/api/shareEmail/" + this.state.id_berita, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
      },
    })
      .then(response => response.json())
      .then((data) => {
        this.setState({
          shareEmail: data.data
        });
      })

    fetch("http://ec2-13-250-50-44.ap-southeast-1.compute.amazonaws.com:9009/api/shareTwitter/" + this.state.id_berita, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
      },
    })
      .then(response => response.json())
      .then((data) => {
        this.setState({
          shareTwitter: data.data
        });
      })

    fetch("http://ec2-13-250-50-44.ap-southeast-1.compute.amazonaws.com:9009/api/share-whatsapp/" + this.state.id_berita, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
      },
    })
      .then(response => response.json())
      .then((data) => {
        this.setState({
          shareWhatsapp: data.data
        });
      })

    if (ITEM_HEIGHT < 700) {
      this.setState({heightModal : "100%"})
    }else{
      this.setState({heightModal : "30%"})
    }

  }

  render() {
    let shareOptionsWA = {
      title: this.state.shareWhatsapp.title,
      message: this.state.shareWhatsapp.title,
      url: this.state.shareWhatsapp.url
    };
    let shareOptionsEmail = {
      title: this.state.shareEmail.newsTitle,
      message: this.state.shareEmail.newsDescription,
      url: this.state.shareEmail.newsImage,
      subject: this.state.shareEmail.newsTitle //  for email
    };
    let shareOptionsFB = {
      title: this.state.shareEmail.newsTitle,
      message: this.state.shareEmail.newsDescription,
      url: this.state.shareEmail.newsImage
    };

    return (
      <Container>

        <AppHeader navigation={this.props.navigation} title="Detail Berita" />

        <Content style={{ backgroundColor: '#fff' }}>
          <Card transparent style={{ padding: '3%', elevation: 0, shadowOpacity: 0, backgroundColor: "transparent" }}>
            <Text style={styles.cardDate} >{this.state.databerita.newsStartDate}</Text>
            <CardItem cardBody>
              <Text style={styles.cardTitle} >{this.state.databerita.newsTitle}</Text>
              <Right>
                <TouchableOpacity onPress={this._showModal}>
                  <Image source={require("../../img/asset/ic_share.png")} />
                </TouchableOpacity>
              </Right>
            </CardItem>
            <CardItem cardBody>
              <Text style={styles.cardAuthor} >{this.state.databerita.newsAuthor}</Text>
            </CardItem>
            <Thumbnail square source={{ uri: this.state.databerita.newsImage }} style={styles.cardImage} />
            <CardItem cardBody>
              <Text style={styles.cardDesc} >{this.state.databerita.newsDescription}</Text>
            </CardItem>
          </Card>

          <Modal isVisible={this.state.isModalVisible} style={{ flex: 1, backgroundColor: "transparent", marginLeft: "65%", marginTop: "-100%" }}
            onBackdropPress={() => this.setState({ isModalVisible: false })}
            onBackButtonPress={() => this.setState({ isModalVisible: false })}>
            <ImageBackground style={{ paddingTop: "25%", marginTop: this.state.heightModal }} source={require("../../img/asset/background_modal.png")}>
              <View style={{ marginBottom: "5%" }}>
                <CardItem cardBody style={{}}>
                  <TouchableHighlight style={{ marginLeft: "8%", marginTop: "5%" }} underlayColor="white"
                    onPress={() => Linking.openURL(this.state.shareFacebook)}>
                    <Image style={{ height: 36, width: 25 }} source={require("../../img/asset/ic_facebook.png")} />
                  </TouchableHighlight>
                  <TouchableHighlight style={{ marginTop: "5%" }} underlayColor="white"
                    onPress={() => Linking.openURL(this.state.shareTwitter)}>
                    <Image style={{ height: 36, width: 24, marginLeft: 7 }} source={require("../../img/asset/ic_twitter.png")} />
                  </TouchableHighlight>
                  <TouchableHighlight style={{ marginTop: "5%" }} underlayColor="white"
                    onPress={() => {
                      setTimeout(() => {
                        Share.shareSingle(Object.assign(shareOptionsWA, {
                          "social": "whatsapp"
                        }));
                      }, 300);
                    }}>
                    <Image style={{ height: 36, width: 25, marginLeft: 7 }} source={require("../../img/asset/ic_whatsapp.png")} />
                  </TouchableHighlight>
                </CardItem>
                <CardItem cardBody>
                  <TouchableHighlight style={{ marginLeft: "3%", marginTop: "3%", marginBottom: "5%" }} underlayColor="white"
                    onPress={() => {
                      this.onCancel();
                      setTimeout(() => {
                        Share.shareSingle(Object.assign(shareOptionsEmail, {
                          "social": "email"
                        }));
                      }, 300);
                    }}>
                    <Image style={{ height: 36, width: 24, marginLeft: 7 }} source={require("../../img/asset/ic_email.png")} />
                  </TouchableHighlight>
                  <TouchableHighlight style={{ marginTop: "3%", marginBottom: "5%" }} underlayColor="white"
                    onPress={() => {
                      this.onCancel();
                      setTimeout(() => {
                        if (typeof shareOptionsWA["title"] !== undefined) {
                          Clipboard.setString(shareOptionsWA["title"]);
                          if (Platform.OS === "android") {
                            ToastAndroid.show('Link copiado al portapapeles', ToastAndroid.SHORT);
                          } else if (Platform.OS === "ios") {
                            AlertIOS.alert('Link copiado al portapapeles');
                          }
                        }
                      }, 300);
                    }}>
                    <Image style={{ height: 36, width: 22, marginLeft: 7 }} source={require("../../img/asset/ic_copy.png")} />
                  </TouchableHighlight>
                </CardItem>
              </View>
            </ImageBackground>
          </Modal>
        </Content>
      </Container>

    );
  }

  _showModal = () => {
    this.setState({ isModalVisible: true })
  }
}

function parseDate(data) {
  if (!data) return "";
  const date = new Date(data);
  let retval = "";
  switch (date.getDay()) {
    case 0: retval += "Senin"; break;
    case 1: retval += "Selasa"; break;
    case 2: retval += "Rabu"; break;
    case 3: retval += "Kamis"; break;
    case 4: retval += "Jumat"; break;
    case 5: retval += "Sabtu"; break;
    case 6: retval += "Minggu"; break;
  }
  retval += ", " + date.getDate();
  switch (date.getMonth()) {
    case 0: retval += " Jan"; break;
    case 1: retval += " Feb"; break;
    case 2: retval += " Mar"; break;
    case 3: retval += " Apr"; break;
    case 4: retval += " Mei"; break;
    case 5: retval += " Jun"; break;
    case 6: retval += " Jul"; break;
    case 7: retval += " Agu"; break;
    case 8: retval += " Sep"; break;
    case 9: retval += " Okt"; break;
    case 10: retval += " Nov"; break;
    case 11: retval += " Des"; break;
  }
  retval += " " + date.getFullYear();
  return retval;
}

const styles = StyleSheet.create({
  cardImage: {
    width: '100%',
    height: 150,
    borderRadius: 3,
    marginTop: '3%',
  },
  cardDate: {
    marginTop: '2%',
    fontFamily: 'Ubuntu-Light',
    fontSize: 10,
  },
  cardTitle: {
    fontFamily: 'Gotham-Bold',
    fontSize: 15,
    width: "90%"
  },
  cardAuthor: {
    fontFamily: 'Ubuntu-Light',
    fontSize: 12,
  },
  cardDesc: {
    marginTop: '2%',
    fontFamily: 'Ubuntu-Light',
    fontSize: 14,
    lineHeight: 16,
  }
});
