import React, { Component } from 'react';
import { Container, Header, Content, Form, Item, Input, Label, Left, Icon, Button } from 'native-base';
import { AppHeader, AppFooter } from '../app-nav/index';
import { AppRegistry, StyleSheet, Text, TextInput, View } from 'react-native';
var Analytics = require('react-native-firebase-analytics');
export default class Zakat extends Component {
  constructor() {
    super()
    this.state = {
      gaji: '', bonus: '', pengeluaran: '',
      hasil: '', beras: ''
    }
  }

  componentWillMount() {
    Analytics.logEvent('zakat', {
      'item_id': 'zakat'
    });
  }

  render() {
    let gaji = this.state.gaji;
    let bonus = this.state.bonus;
    let pengeluaran = this.state.pengeluaran;
    let haul = this.state.beras * 520;
    let limit = gaji && bonus && pengeluaran ? ((gaji + bonus - pengeluaran)) : null;
    let result = limit * 0.025;
    if (haul > limit) {
      result = "Anda Tidak Wajib Membayar Zakat";
    }
    return (

      <Container style={{ backgroundColor: "#FFF" }}>

        <AppHeader navigation={this.props.navigation} title="Kalkulator Zakat" />

        <Content>
          <Text style={styles.titleText}>Data Harta</Text>
          <Form style={{marginTop:"10%"}}>
            <Item stackedLabel style={{ marginRight: "5%", marginBottom : "3%" }}>
              <Label style={styles.txtLabel}>Penghasilan bersih (Rp)</Label>
              <Input style={styles.txtPlaceholder} placeholder="Jumlah Penghasilan Bersih" keyboardType='numeric' onChangeText={(text) => this.setState({ gaji: parseInt(text) })} />
            </Item>
            <Item stackedLabel style={{ marginRight: "5%", marginBottom : "3%" }}>
              <Label style={styles.txtLabel}>Penghasilan lain (Rp)</Label>
              <Input placeholder="Jumlah Penghasilan Bersih" keyboardType='numeric' onChangeText={(text) => this.setState({ bonus: parseInt(text) })} />
            </Item>
            <Item stackedLabel style={{ marginRight: "5%", marginBottom : "3%" }}>
              <Label style={styles.txtLabel}>Pengeluaran (Rp)</Label>
              <Input placeholder="Jumlah Pengeluaran" keyboardType='numeric' onChangeText={(text) => this.setState({ pengeluaran: parseInt(text) })} />
            </Item>
            <Item stackedLabel style={{marginRight: "5%", marginBottom : "3%" }}>
              <Label style={styles.txtLabel}>Harga Beras Saat Ini (per kg)</Label>
              <Input placeholder="Harga Beras" keyboardType='numeric' onChangeText={(text) => this.setState({ beras: parseInt(text) })} />
            </Item>
          </Form>
          <Item rounded style={{ marginTop: "10%", marginLeft: "5%", marginRight: "5%" }}>
            <Input disabled placeholder='Harta Zakat' />{result ? <Text> {result} </Text> : null}
          </Item>
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  baseText: {
    fontFamily: 'Ubuntu-Light',

  },
  titleText: {
    fontFamily: 'GothamRounded-Bold',
    fontSize: 30,
    fontWeight: 'bold',
    marginTop: "5%",
    marginLeft: "2%"
  },
  container: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentContainer: {
    borderWidth: 0,
    borderColor: '#FFF',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    backgroundColor: "#fff"
  },
  txtPlaceholder :{
    fontFamily: 'Ubuntu-Light',
    color : '#363636',
    
  },
  txtLabel : {
    fontFamily: 'Ubuntu-Regular',
    color : '#888888'
  }
});