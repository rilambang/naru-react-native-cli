import React from "react";
import { AppRegistry, View, StyleSheet, StatusBar, Image,Dimensions, ImageBackground, Linking, TouchableOpacity, TouchableHighlight, ActivityIndicator } from "react-native";
import {
  Button,
  Text,
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Left,
  Badge,
  Right,
  Icon,
  Title,
  Input,
  InputGroup,
  Item,
  Tab,
  Tabs,
  Footer,
  FooterTab,
  Label,
  Thumbnail,
  ListItem,
  CheckBox,
  Picker
} from "native-base";
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import HTMLView from 'react-native-htmlview';
import { AppHeader, AppFooter } from '../app-nav/index';
import Share, { ShareSheet } from 'react-native-share';
import Modal from 'react-native-modal';
const ITEM_HEIGHT = Dimensions.get('window').height;
var Analytics = require('react-native-firebase-analytics');


export default class promoDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      databerita: '',
      id_berita: this.props.navigation.state.params.idBerita,
      isReady: false,
      shareFacebook: '',
      shareTwitter: '',
      shareEmail: [],
      shareWA: [],
      isModalVisible: false
    };
  }

  componentWillMount() {
    Analytics.logEvent('promo_detail', {
      'item_id': this.state.id_berita
    });
  }

  componentDidMount() {
    fetch("http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000/listpromos/" + this.state.id_berita, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then((data) => {
        if (data.length != 'undefined') {
          this.setState({ databerita: data });
          this.setState({ isReady: true });
        }
      })

    fetch("http://ec2-13-250-50-44.ap-southeast-1.compute.amazonaws.com:9009/api/shareFacebook", {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
      },
    })
      .then(response => response.json())
      .then((data) => {
        this.setState({
          shareFacebook: data.data
        });
      })

    fetch("http://ec2-13-250-50-44.ap-southeast-1.compute.amazonaws.com:9009/api/promo-share-whatsapp/" + this.state.id_berita, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
      },
    })
      .then(response => response.json())
      .then((data) => {
        this.setState({
          shareWA: data.data
        });
      })

    fetch("http://ec2-13-250-50-44.ap-southeast-1.compute.amazonaws.com:9009/api/promo-share-email/" + this.state.id_berita, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
      },
    })
      .then(response => response.json())
      .then((data) => {
        this.setState({
          shareEmail: data.data
        });
      })
    fetch("http://ec2-13-250-50-44.ap-southeast-1.compute.amazonaws.com:9009/api/promo-share-twitter/" + this.state.id_berita, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
      },
    })
      .then(response => response.json())
      .then((data) => {
        this.setState({
          shareTwitter: data.data
        });
      })

    if (ITEM_HEIGHT < 700) {
      this.setState({ heightModal: "100%" })
    } else {
      this.setState({ heightModal: "30%" })
    }
  }

  render() {
    let shareOptionsWA = {
      title: this.state.shareWA.title,
      message: this.state.shareWA.title,
      url: this.state.shareWA.url
    };
    let shareOptionsEmail = {
      title: this.state.shareEmail.promoTitle,
      message: this.state.shareEmail.promoDescription,
      url: this.state.shareEmail.url,
      subject: this.state.shareEmail.promoTitle//  for email
    };
    let shareOptionsFB = {
      title: this.state.shareEmail.promoTitle,
      message: this.state.shareEmail.promoDescription,
      url: this.state.shareFacebook
    };
    if (!this.state.isReady) {
      return (
        <ActivityIndicator size="small" color="#00ff00" />
      )
    }
    return (
      <Container>

        <AppHeader navigation={this.props.navigation} title="Detail Promo" />

        <Content style={{ backgroundColor: '#fff' }}>
          <Card transparent style={{ padding: '3%', elevation: 0, shadowOpacity: 0, backgroundColor: "transparent" }}>
            <Text style={styles.cardDate} >{this.state.databerita.liststartDate}</Text>
            <CardItem cardBody>
              <Text style={styles.cardTitle} >{this.state.databerita.listNewsPromoName}</Text>
              <Right>
                <TouchableOpacity onPress={this._showModal}>
                  <Image source={require("../../img/asset/ic_share.png")} />
                </TouchableOpacity>
              </Right>
            </CardItem>
            <CardItem cardBody>
              <TouchableHighlight style={{ marginTop: "3%" }} underlayColor="white"
                onPress={() => Linking.openURL(this.state.shareFacebook)}>
                <Image style={{ height: 36, width: 25 }} source={require("../../img/asset/ic_facebook.png")} />
              </TouchableHighlight>
              <TouchableHighlight style={{ marginTop: "3%" }} underlayColor="white"
                onPress={() => Linking.openURL(this.state.shareTwitter)}>
                <Image style={{ height: 36, width: 24, marginLeft: 7 }} source={require("../../img/asset/ic_twitter.png")} />
              </TouchableHighlight>
              <TouchableHighlight style={{ marginTop: "3%" }} underlayColor="white"
                onPress={() => {
                  setTimeout(() => {
                    Share.shareSingle(Object.assign(shareOptionsWA, {
                      "social": "whatsapp"
                    }));
                  }, 300);
                }} >
                <Image style={{ height: 36, width: 25, marginLeft: 7 }} source={require("../../img/asset/ic_whatsapp.png")} />
              </TouchableHighlight>
              <TouchableHighlight style={{ marginTop: "3%" }} underlayColor="white"
                onPress={() => {
                  setTimeout(() => {
                    Share.shareSingle(Object.assign(shareOptionsEmail, {
                      "social": "email"
                    }));
                  }, 300);
                }}>
                <Image style={{ height: 36, width: 24, marginLeft: 7 }} source={require("../../img/asset/ic_email.png")} />
              </TouchableHighlight>
              <TouchableHighlight style={{ marginTop: "3%" }} underlayColor="white"
                onPress={() => {
                  setTimeout(() => {
                    if (typeof shareOptionsWA["title"] !== undefined) {
                      Clipboard.setString(shareOptionsWA["title"]);
                      if (Platform.OS === "android") {
                        ToastAndroid.show('Link copiado al portapapeles', ToastAndroid.SHORT);
                      } else if (Platform.OS === "ios") {
                        AlertIOS.alert('Link copiado al portapapeles');
                      }
                    }
                  }, 300);
                }}>
                <Image style={{ height: 36, width: 22, marginLeft: 7 }} source={require("../../img/asset/ic_copy.png")} />
              </TouchableHighlight>
            </CardItem>
            {
              this.state.databerita.listNewsPromoCdn.map((item, idx) => {
                return <Thumbnail key={idx} square source={{ uri: item }} style={styles.cardImage} />;
              })
            }
            <CardItem cardBody>
              <HTMLView
                value={this.state.databerita.listNewsPromoDescription}
                stylesheet={htmlstyles}
              />
            </CardItem>
          </Card>

          <Modal isVisible={this.state.isModalVisible} style={{ flex: 1, backgroundColor: "transparent", marginLeft: "65%", marginTop: "-100%" }}
            onBackdropPress={() => this.setState({ isModalVisible: false })}
            onBackButtonPress={() => this.setState({ isModalVisible: false })}>
            <ImageBackground style={{ paddingTop: "25%", marginTop: this.state.heightModal }} source={require("../../img/asset/background_modal.png")}>
              <View style={{ marginBottom: "5%" }}>
                <CardItem cardBody style={{}}>
                  <TouchableHighlight style={{ marginLeft: "8%", marginTop: "5%" }} underlayColor="white"
                    onPress={() => Linking.openURL(this.state.shareFacebook)}>
                    <Image style={{ height: 36, width: 25 }} source={require("../../img/asset/ic_facebook.png")} />
                  </TouchableHighlight>
                  <TouchableHighlight style={{ marginTop: "5%" }} underlayColor="white"
                    onPress={() => Linking.openURL(this.state.shareTwitter)}>
                    <Image style={{ height: 36, width: 24, marginLeft: 7 }} source={require("../../img/asset/ic_twitter.png")} />
                  </TouchableHighlight>
                  <TouchableHighlight style={{ marginTop: "5%" }} underlayColor="white"
                    onPress={() => {
                      setTimeout(() => {
                        Share.shareSingle(Object.assign(shareOptionsWA, {
                          "social": "whatsapp"
                        }));
                      }, 300);
                    }}>
                    <Image style={{ height: 36, width: 25, marginLeft: 7 }} source={require("../../img/asset/ic_whatsapp.png")} />
                  </TouchableHighlight>
                </CardItem>
                <CardItem cardBody>
                  <TouchableHighlight style={{ marginLeft: "3%", marginTop: "3%", marginBottom: "5%" }} underlayColor="white"
                    onPress={() => {
                      this.onCancel();
                      setTimeout(() => {
                        Share.shareSingle(Object.assign(shareOptionsEmail, {
                          "social": "email"
                        }));
                      }, 300);
                    }}>
                    <Image style={{ height: 36, width: 24, marginLeft: 7 }} source={require("../../img/asset/ic_email.png")} />
                  </TouchableHighlight>
                  <TouchableHighlight style={{ marginTop: "3%", marginBottom: "5%" }} underlayColor="white"
                    onPress={() => {
                      this.onCancel();
                      setTimeout(() => {
                        if (typeof shareOptionsWA["title"] !== undefined) {
                          Clipboard.setString(shareOptionsWA["title"]);
                          if (Platform.OS === "android") {
                            ToastAndroid.show('Link copiado al portapapeles', ToastAndroid.SHORT);
                          } else if (Platform.OS === "ios") {
                            AlertIOS.alert('Link copiado al portapapeles');
                          }
                        }
                      }, 300);
                    }}>
                    <Image style={{ height: 36, width: 22, marginLeft: 7 }} source={require("../../img/asset/ic_copy.png")} />
                  </TouchableHighlight>
                </CardItem>
              </View>
            </ImageBackground>
          </Modal>
        </Content>
      </Container>
    );
  }
  _showModal = () => {
    this.setState({ isModalVisible: true })
  }
}

function parseDate(data) {
  if (!data) return "";
  const date = new Date(data);
  let retval = "";
  switch (date.getDay()) {
    case 0: retval += "Senin"; break;
    case 1: retval += "Selasa"; break;
    case 2: retval += "Rabu"; break;
    case 3: retval += "Kamis"; break;
    case 4: retval += "Jumat"; break;
    case 5: retval += "Sabtu"; break;
    case 6: retval += "Minggu"; break;
  }
  retval += ", " + date.getDate();
  switch (date.getMonth()) {
    case 0: retval += " Jan"; break;
    case 1: retval += " Feb"; break;
    case 2: retval += " Mar"; break;
    case 3: retval += " Apr"; break;
    case 4: retval += " Mei"; break;
    case 5: retval += " Jun"; break;
    case 6: retval += " Jul"; break;
    case 7: retval += " Agu"; break;
    case 8: retval += " Sep"; break;
    case 9: retval += " Okt"; break;
    case 10: retval += " Nov"; break;
    case 11: retval += " Des"; break;
  }
  retval += " " + date.getFullYear();
  return retval;
}

const htmlstyles = StyleSheet.create({
  p: {
    marginTop: '2%',
    fontFamily: 'Ubuntu-Light',
    fontSize: 14,
    lineHeight: 16,
  }
});

const styles = StyleSheet.create({
  cardImage: {
    width: '100%',
    height: 150,
    borderRadius: 3,
    marginTop: '3%',
  },
  cardDate: {
    marginTop: '2%',
    fontFamily: 'Ubuntu-Light',
    fontSize: 10,
  },
  cardTitle: {
    fontFamily: 'GothamRounded-Bold',
    fontSize: 15,
  },
  cardAuthor: {
    fontFamily: 'Ubuntu-Light',
    fontSize: 12,
  },
});