import React, { Component } from 'react';
import { View, StyleSheet, AsyncStorage, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Thumbnail, Text, Body, Left, Right, List, ListItem } from 'native-base';

export default class Kuliner extends Component {
  constructor(props) {
    super(props)
    this.state = {
      resto: [],
      isReady: false,
      property: this.props,
      textShowAll: "Lihat Semua",
      jmlLoad: 5
    }
  }
  componentDidMount() {
    const baseUrl = "http://ec2-54-255-226-10.ap-southeast-1.compute.amazonaws.com:9009/api/foods";
    fetchData(baseUrl).then(data => {
      this.setState({ resto: data, isReady: true });
    });
  }

  seeAll = () => {
    if (this.state.textShowAll === "Lihat Semua") {
      this.setState({ textShowAll: "Sembunyikan", jmlLoad: 10 });
    } else {
      this.setState({ textShowAll: "Lihat Semua", jmlLoad: 5 });
    }
  }

  render() {
    const resto = this.state.resto.data;
    if (!this.state.isReady) {
      return (
        <ActivityIndicator size="small" color="#00ff00" />
      )
    }
    else if (typeof resto == 'undefined' || resto.length == 0) {
      return (<View>
        <Text style={{ alignItems: "center", alignSelf: "center", marginTop: "5%" }}>Data Tidak Ada</Text>
      </View>);
    } else {
      return (
        <View>
          <List>
            {
              resto.slice(0, this.state.jmlLoad).map((item, idx) => {
                if (item.opening_hours) {
                  if (item.opening_hours.open_now == true) {
                    opening = "Buka";
                  } else {
                    opening = "Tutup";
                  }
                } else {
                  opening = "-";
                }
                return (
                  <TouchableOpacity key={idx} onPress={() => this.state.property.navigation.navigate("Peta", { location: item })}>
                    <View style={{ flex: 1, flexDirection: 'column', borderBottomWidth: 1, borderColor: "#979797", marginTop: "1%" }}>
                      <Text style={styles.wordTitle}>{item.name}</Text>
                      <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Thumbnail square source={require('../../img/asset/home/lokasi.png')} style={styles.imageBerita} />
                        <Body style={styles.wordBerita}>
                          <Text style={styles.wordAddress}>{sliceAddress(item.location)}</Text>
                          <Text style={styles.wordRange}>{opening}</Text>
                        </Body>
                      </View>
                    </View>
                  </TouchableOpacity>
                )
              })
            }
          </List>
          <View style={styles.viewSeeAll}>
            <TouchableOpacity onPress={this.seeAll}>
              <Text style={styles.wordSeeAll}>{this.state.textShowAll}</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
  }
}



function fetchData(url) {
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
          },
          body: JSON.stringify({ gps: position.coords.latitude + ',' + position.coords.longitude, radius: "500", types: "restaurant" })
        })
          .then(response => response.json())
          .then((data) => {
            resolve(data);
          })
          .catch((e) => {
            reject(e);
          })
      })
  })
}

function sliceAddress(text) {
  return text.length > 50 ? text.substring(0, 50) + "..." : text;
}

const styles = StyleSheet.create({
  imageBerita: {
    borderRadius: 3,
    marginLeft: "3%",
    marginRight: "3%",
    marginBottom: "3%",
    width: 30,
    height: 30,
  },
  wordBerita: {
    alignItems: 'flex-start',
  },
  wordRange: {
    fontFamily: 'Ubuntu-Regular',
    fontSize: 10,
    color: "#363636",
  },
  wordTitle: {
    paddingLeft: "3%",
    paddingRight: "3%",
    fontFamily: 'Gotham-Medium',
    fontSize: 16,
  },
  wordAddress: {
    fontFamily: 'Ubuntu-Light',
    fontSize: 12,
  },
  wordSeeAll: {
    fontFamily: 'Ubuntu-Light',
    fontSize: 12,
    textAlign: 'center',
    borderRadius: 6,
    borderWidth: 2,
    borderColor: '#d8d8d8',
    padding: 10,
    paddingLeft: 10,
    paddingRight: 10
  },
  viewSeeAll: {
    paddingTop: "2%",
    alignItems: "center",
    backgroundColor: "#ffffff",
    paddingBottom: "2%",
  }

});