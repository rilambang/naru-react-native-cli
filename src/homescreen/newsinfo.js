import React, { Component } from 'react';
import { View, StyleSheet, AsyncStorage, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Thumbnail, Text, Body, Left, Right, List, ListItem } from 'native-base';

export default class Newsinfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      property: this.props,
      dataNews: [],
      isReady: false,
      dataNewsAll: [],
      textShowAll: "Lihat Semua",
      jmlLoad: 5
    }
  }
  componentDidMount() {
    const baseUrl = "http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000/news_event_top";
    fetchData(baseUrl).then(data => {
      this.setState({ dataNews: data, isReady: true });
    });
  }
  render() {
    const dataNews = this.state.dataNews;
    if (!this.state.isReady) {
      return (
        <ActivityIndicator size="small" color="#00ff00" />
      )
    }else if (typeof dataNews == 'undefined' || dataNews.length == 0) {
      return (
        <Text style={styles.wordTitle}>Tidak ada berita</Text>
      )
    } else {
      return (
        <View>
          <List>
            {dataNews.slice(0, this.state.jmlLoad).map((item, idx) => {
              return (
                <TouchableOpacity key={idx} onPress={() => this.state.property.navigation.navigate("NewsDetail", { idBerita: item._id })}>
                  <View style={{ flex: 1, flexDirection: 'row', borderColor: '#f8f8f8', borderBottomWidth: 2 }}>
                    <Thumbnail square source={{ uri: item.newsImage }} style={styles.imageBerita} />
                    <Body style={styles.wordBerita}>
                      <Text style={styles.wordTitle}>{sliceNewsTitle(item.newsTitle)}</Text>
                      <Text style={styles.wordDate}>{item.newsStartDate}</Text>
                      <Text style={styles.wordDesc}>{sliceNewsDescription(item.newsDescription)}</Text>
                    </Body>
                  </View>
                </TouchableOpacity>
              )
            })}
          </List>
          <View style={styles.viewSeeAll}>
            <TouchableOpacity onPress={this.seeAll}>
              <Text style={styles.wordSeeAll}>{this.state.textShowAll}</Text>
            </TouchableOpacity>
          </View>
        </View>
      )
    }
  }

  seeAll = () => {
    if (this.state.textShowAll === "Lihat Semua") {
      this.setState({ textShowAll: "Sembunyikan", jmlLoad: 10 });
    } else {
      this.setState({ textShowAll: "Lihat Semua", jmlLoad: 5 });
    }
  }

}

function fetchData(url) {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem("news", (error, result) => {
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      })
        .then(response => response.json())
        .then((data) => {
          resolve(data);
        })
        .catch((e) => {
          reject(e);
        })
    })
  })
}

function ListContent(props) {
  const news = props.news.dataNews;
  if (typeof news == 'undefined') {
    return (
      <Text style={styles.wordTitle}>Tidak ada berita</Text>
    )
  }
  return (
    <View>
      {
        news.length == 0 ?
          (
            <Text style={styles.wordTitle}>Tidak ada berita</Text>
          ) : (
            <List>
              {news.slice(0, props.jmlLoad).map((item, idx) => {
                return (
                  <TouchableOpacity key={idx} onPress={() => props.news.property.navigation.navigate("NewsDetail", { idBerita: item._id })}>
                    <View style={{ flex: 1, flexDirection: 'row', borderColor: '#f8f8f8', borderBottomWidth: 2 }}>
                      <Thumbnail square source={{ uri: item.newsImage }} style={styles.imageBerita} />
                      <Body style={styles.wordBerita}>
                        <Text style={styles.wordTitle}>{sliceNewsTitle(item.newsTitle)}</Text>
                        <Text style={styles.wordDate}>{item.newsStartDate}</Text>
                        <Text style={styles.wordDesc}>{sliceNewsDescription(item.newsDescription)}</Text>
                      </Body>
                    </View>
                  </TouchableOpacity>
                )
              })}
            </List>
          )
      }
    </View>
  )
}


function parseDate(data) {
  if (!data) return "";
  const date = new Date(data);
  let retval = "";
  switch (date.getDay()) {
    case 0: retval += "Senin"; break;
    case 1: retval += "Selasa"; break;
    case 2: retval += "Rabu"; break;
    case 3: retval += "Kamis"; break;
    case 4: retval += "Jumat"; break;
    case 5: retval += "Sabtu"; break;
    case 6: retval += "Minggu"; break;
  }
  retval += ", " + date.getDate();
  switch (date.getMonth()) {
    case 0: retval += " Jan"; break;
    case 1: retval += " Feb"; break;
    case 2: retval += " Mar"; break;
    case 3: retval += " Apr"; break;
    case 4: retval += " Mei"; break;
    case 5: retval += " Jun"; break;
    case 6: retval += " Jul"; break;
    case 7: retval += " Agu"; break;
    case 8: retval += " Sep"; break;
    case 9: retval += " Okt"; break;
    case 10: retval += " Nov"; break;
    case 11: retval += " Des"; break;
  }
  retval += " " + date.getFullYear();
  return retval;
}

function sliceNewsTitle(text) {
  return text.length > 50 ? text.substring(0, 50) + "..." : text;
}

function sliceNewsDescription(text) {
  return text.length > 60 ? text.substring(0, 60) + "..." : text;
}

const styles = StyleSheet.create({
  imageBerita: {
    borderRadius: 3,
    margin: "3%",
    width: 100,
    height: 70,
  },
  wordBerita: {
    alignItems: 'flex-start',
    margin: "2%",
  },
  wordDate: {
    fontFamily: 'Ubuntu-Light',
    fontSize: 10,
    color: "#363636",
  },
  wordTitle: {
    fontFamily: 'Ubuntu-Regular',
    fontSize: 14,
  },
  wordDesc: {
    marginTop: '2%',
    fontFamily: 'Ubuntu-Light',
    fontSize: 12,
  },
  wordSeeAll: {
    fontFamily: 'Ubuntu-Light',
    fontSize: 12,
    textAlign: 'center',
    borderRadius: 6,
    borderWidth: 2,
    borderColor: '#d8d8d8',
    padding: 10,
    paddingLeft: 10,
    paddingRight: 10
  },
  viewSeeAll: {
    paddingTop: "2%",
    alignItems: "center",
    backgroundColor: "#ffffff",
    paddingBottom: "2%",
  }

});