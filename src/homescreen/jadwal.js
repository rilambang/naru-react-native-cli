import React, { Component } from 'react';
import { View, StyleSheet, AsyncStorage, ActivityIndicator } from 'react-native';
import { Thumbnail, Text, Body, Left, Right, List, ListItem, Button } from 'native-base';

export default class Jadwal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      jadwalSholat: [],
      isReady: false,
    }
  }
  componentDidMount() {
    const baseUrl = "http://ec2-54-255-226-10.ap-southeast-1.compute.amazonaws.com:9009/api/pray";
    fetchData(baseUrl).then(data => {
      this.setState({ jadwalSholat: data, isReady: true });
      AsyncStorage.setItem("subuh", this.state.jadwalSholat.data.subuh);
      AsyncStorage.setItem("dzuhur", this.state.jadwalSholat.data.dzuhur);
      AsyncStorage.setItem("ashar", this.state.jadwalSholat.data.ashar);
      AsyncStorage.setItem("maghrib", this.state.jadwalSholat.data.maghrib);
      AsyncStorage.setItem("isya", this.state.jadwalSholat.data.isya);
    });
    // AsyncStorage.setItem("dzuhur", JSON.stringify(this.state.jadwalSholat.data.dzuhur));
  }
  render() {
    const jadwalSholat = this.state.jadwalSholat;
    if (!this.state.isReady) {
     return (
        <ActivityIndicator size="small" color="#00ff00" />
      )
    } else {
      return (
        <View>
          <View style={styles.viewStyle}>
            <Button bordered style={styles.buttonStyle}>
              <Thumbnail square source={require('../../img/asset/home/imsak.png')} style={{ width: 35, height: 40, }} />
              <Text style={styles.textButton}>Imsak</Text>
              <Text style={styles.wordAddress}>{jadwalSholat.data.imsak}</Text>
            </Button>
            <Button bordered style={styles.buttonStyle}>
              <Thumbnail square source={require('../../img/asset/home/subuh.png')} style={{ width: 35, height: 20, marginTop: 20 }} />
              <Text style={styles.textButton}>Subuh</Text>
              <Text style={styles.wordAddress}>{jadwalSholat.data.subuh}</Text>
            </Button>
            <Button bordered style={styles.buttonStyle}>
              <Thumbnail square source={require('../../img/asset/home/dzuhur.png')} style={{ width: 40, height: 40, }} />
              <Text style={styles.textButton}>Dzuhur</Text>
              <Text style={styles.wordAddress}>{jadwalSholat.data.dzuhur}</Text>
            </Button>
          </View>
          <View style={styles.viewStyle}>
            <Button bordered style={styles.buttonStyle}>
              <Thumbnail square source={require('../../img/asset/home/ashar.png')} style={{ width: 40, height: 40, }} />
              <Text style={styles.textButton}>Ashar</Text>
              <Text style={styles.wordAddress}>{jadwalSholat.data.ashar}</Text>
            </Button>
            <Button bordered style={styles.buttonStyle}>
              <Thumbnail square source={require('../../img/asset/home/maghrib.png')} style={{ width: 40, height: 20, marginTop: 20 }} />
              <Text style={styles.textButton}>Maghrib</Text>
              <Text style={styles.wordAddress}>{jadwalSholat.data.maghrib}</Text>
            </Button>
            <Button bordered style={styles.buttonStyle}>
              <Thumbnail square source={require('../../img/asset/home/isya.png')} style={{ width: 35, height: 40, }} />
              <Text style={styles.textButton}>Isya</Text>
              <Text style={styles.wordAddress}>{jadwalSholat.data.isya}</Text>
            </Button>
          </View>
        </View>
      );
    }
  }
}

function fetchData(url) {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem("news", (error, result) => {
      AsyncStorage.getItem("latitude", (error, latitude) => {
        AsyncStorage.getItem("longitude", (error, longitude) => {
          fetch(url, {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
            },
            body: JSON.stringify({ latitude: latitude, longitude: longitude })
          })
            .then(response => response.json())
            .then((data) => {
              resolve(data);
            })
            .catch((e) => {
              reject(e);
            })
        })
      })
    })
  })
}

const styles = StyleSheet.create({
  viewStyle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: "2%",
  },
  buttonStyle: {
    marginRight: '3%',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    borderWidth: 2,
    borderColor: "#d8d8d8",
    width: "32%",
    height: 104,
  },
  textButton: {
    fontFamily: "Gotham-Light",
    color: "#000000",
    fontSize: 10,
  },
  imageBerita: {
    paddingTop: '3%',
  },
  wordBerita: {
    alignItems: 'flex-start',
  },
  wordRange: {
    fontFamily: 'Ubuntu-Regular',
    fontSize: 10,
    color: "#363636",
  },
  wordTitle: {
    paddingLeft: "3%",
    paddingRight: "3%",
    fontFamily: 'Gotham-Medium',
    fontSize: 16,
  },
  wordAddress: {
    fontFamily: '-Light',
    fontSize: 12,
    color: '#000',
  },

});