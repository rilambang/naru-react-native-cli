import React, { Component } from "react";
import Home from "../screen/home.js";
import PromoMenu from "../screen/PromoMenu.js";
import News from "../screen/News.js";
import Poi from "../screen/poi.js";
import Kontak from "../screen/Kontak.js";
//import screenIndex from "../screen/index.js"

import { TabNavigator } from "react-navigation";
import {
  StyleProvider,
  Button,
  Text,
  Icon,
  Item,
  Footer,
  FooterTab,
  Label
} from "native-base";
import getTheme from "./../../native-base-theme/components/";
import customColor from "./../../native-base-theme/variables/customColor";

export default (MainScreenNavigator = TabNavigator(
  {
    Home: {
      screen: Home,
      headerMode: 'none',
      header: null,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    PromoMenu: { screen: PromoMenu,
      headerMode: 'none',
      header: null,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    News: { screen: News,
      headerMode: 'none',
      header: null,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    Poi: { screen: Poi,
      headerMode: 'none',
      header: null,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    },
    Kontak: { screen: Kontak,
      headerMode: 'none',
      header: null,
      navigationOptions: {
        header: null,
        gesturesEnabled: false
      }
    }
  },
  {
    tabBarPosition: "bottom",
    tabBarComponent: props => {
      return (
        <StyleProvider style={getTheme(customColor)}>
          <Footer>
            <FooterTab tabStyle={{ backgroundColor: 'red' }}
              textStyle={{ color: '#fff' }}
              activeTabStyle={{ backgroundColor: 'red' }}
              activeTextStyle={{ color: '#fff', fontWeight: 'normal' }}>
              <Button
                vertical
                active={props.navigationState.index === 0}
                onPress={() => props.navigation.navigate("Home")}
              >
                <Icon name="ios-home-outline" />

              </Button>
              <Button
                vertical
                active={props.navigationState.index === 1}
                onPress={() => props.navigation.navigate("PromoMenu")}
              >
                <Icon name="ios-ribbon-outline" />

              </Button>
              <Button
                vertical
                active={props.navigationState.index === 2}
                onPress={() => props.navigation.navigate("News")}
              >
                <Icon name="ios-paper-outline" />

              </Button>
              <Button
                vertical
                active={props.navigationState.index === 3}
                onPress={() => props.navigation.navigate("Poi")}>

                <Icon name="ios-pin-outline" />

              </Button>
              <Button
                vertical
                active={props.navigationState.index === 4}
                onPress={() => props.navigation.navigate("Kontak")}>

                <Icon name="ios-call-outline" />

              </Button>
            </FooterTab>
          </Footer>
        </StyleProvider>
      );
    }
  }
));
